'use strict';
var myApp = angular.module('myApp', ['ngRoute', 'ui.bootstrap', 'googlechart']);

myApp.config(function($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: '/src/views/filmsDetail.html',
            controller: 'filmsDetailCtrl'

        })
        .when('/detail/:id', {
            templateUrl: '/src/views/detail.html',
            controller: 'detailCtrl'

        })
        .when('/chart', {
            templateUrl: '/src/views/chart.html',
            controller: 'chartCtrl'

        })
        .otherwise({
            redirectTo: '/'
        });

});
