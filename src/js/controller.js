'use strict';
var myApp = angular.module('myApp');

myApp.controller('filmsDetailCtrl', ['$scope', '$http', '$rootScope', '$routeParams', '$log', function($scope, $http, $rootScope, $routeParams, $log) {
    var data = {
        "query": {
            "match_all": {}
        },
        "size": 25,
        "sort": [{
            "Year": {
                "order": "desc",
                "mode": "avg"
            }
        }, {
            "imdbVotes": {
                "order": "desc",
                "mode": "avg"
            }
        }]

    }
    $rootScope.film = {};
    var film = {};
    var next = {};
    var prev = {};

    $scope.m = {
        min: 0,
        max: 10
    };

    var isSearch = false;
    var isFilter = false;

    $scope.topis = " ";
    $scope.progress = 0;
    $rootScope.dene = "  ";
    var prevCount = 0;
    var preSearchCount = 0;
    var preFilterCount = 0;
    $scope.pageNum = 1;

    $scope.maxSize = 5;

    $scope.bigCurrentPage = 1;

    $http.post("http://localhost:9200/_search", data).success(function(res) {
        $scope.test = res;
        $scope.bigTotalItems = $scope.test.hits.total;

    })


    $scope.getPageNum = function(num) {
        $scope.pageNum = num;
        $scope.progress = ($scope.bigCurrentPage / $scope.bigTotalItems) * 1000 * 2.5;
        if (isSearch) {
            preSearchCount = num * 25;
            $scope.saveName();
        } else if (isFilter) {
            preFilterCount = num * 25;

            $scope.filterFilms();
        } else {
            prevCount = num * 25;
            $scope.pageNum = num;
            next = {
                "from": prevCount,
                "size": 25,
                "query": {
                    "match_all": {}
                },
                "sort": [{
                    "imdbVotes": {
                        "order": "desc",
                        "mode": "avg"
                    }
                }, {
                    "Year": {
                        "order": "desc",
                        "mode": "avg"
                    }
                }]
            }

            $http.post("http://localhost:9200/_search", next).success(function(temp2) {
                $scope.test = temp2;
                $scope.bigTotalItems = $scope.test.hits.total;
            })

        }
    }


    $scope.saveName = function() {
        isSearch = true;

        $scope.searchText = "Search Results Showing....." + $scope.filmName;
        $scope.searchText2 = "Search Page";
        film = {
            "from": preSearchCount,
            "size": 25,

            "query": {

                "bool": {
                    "should": [{
                            "match": {
                                "Title": $scope.filmName
                            }
                        }, {
                            "match": {
                                "Director": $scope.filmName
                            }
                        }


                    ],
                    "minimum_should_match": 1
                }
            }

        }


        $http.post("http://localhost:9200/_search", film).success(function(temp) {
            $scope.test = temp;
            $scope.bigTotalItems = $scope.test.hits.total;
        })
        if ($scope.test.hits.total === 0) {

            $scope.searchText = "There is no result pleace return back....."

        }
        $scope.SuggestFunc();


    }
    $scope.nextPage = function() {
        $scope.progress = ($scope.bigCurrentPage / $scope.bigTotalItems) * 1000 * 2.5;
        if (isSearch == true) {
            preSearchCount += 25;
            $scope.saveName();
        } else if (isFilter) {
            preFilterCount += 25;
            $scope.filterFilms();
        } else {

            prevCount += 25;
            $scope.pageNum++;
            next = {
                "from": prevCount,
                "size": 25,
                "query": {
                    "match_all": {}
                },
                "sort": [{
                    "Year": {
                        "order": "desc",
                        "mode": "avg"
                    }
                }, {
                    "imdbVotes": {
                        "order": "desc",
                        "mode": "avg"
                    }
                }]
            }

            $http.post("http://localhost:9200/_search", next).success(function(temp2) {
                $scope.test = temp2;
                $scope.bigTotalItems = $scope.test.hits.total;
            })
        }
    };

    $scope.prevPage = function() {
        $scope.progress = ($scope.bigCurrentPage / $scope.bigTotalItems) * 1000 * 2.5;
        if (isSearch && preSearchCount >= 25) {
            preSearchCount -= 25;
            $scope.saveName();
        } else if (isFilter && preFilterCount >= 25) {
            preFilterCount -= 25;
            $scope.filterFilms();
        } else {
            if (prevCount >= 25) {


                prevCount -= 25;
                $scope.pageNum--;
                prev = {
                    "from": prevCount,
                    "size": 25,
                    "query": {
                        "match_all": {}
                    },
                    "sort": [{
                        "Year": {
                            "order": "desc",
                            "mode": "avg"
                        }
                    }, {
                        "imdbVotes": {
                            "order": "desc",
                            "mode": "avg"
                        }
                    }]
                }
                $http.post("http://localhost:9200/_search", prev).success(function(temp3) {
                    $scope.test = temp3;
                    $scope.bigTotalItems = $scope.test.hits.total;
                })
            }
        }
    };
    $scope.sendFilm = function(sfilm) {
        $routeParams.id = sfilm
            //window.location="#/detail/"+sfilm;

    }


    $scope.filter = [];
    $scope.filterFilms = function() {
        isFilter = true;
        $scope.isFilterText = "Filter Page"



        var genre2 = {


            "query": {
                "match_all": {}
            },
            "from": preFilterCount,
            "size": 25,

            "filter": {
                "bool": {
                    "must": []



                }
            }
        }

        for (var i = $scope.filter.length - 1; i >= 0; i--) {
            var data = {
                "term": {
                    "Genre": $scope.filter[i]
                }
            };
            genre2.filter.bool.must.push(data);
            //console.log("girdi");

        };
        if ($scope.votes) {
            var data = {
                "term": {
                    "imdbVotes": $scope.votes
                }
            };
            genre2.filter.bool.must.push(data);
        }
        if ($scope.year) {
            var data = {
                "term": {
                    "Year": $scope.year
                }
            };
            genre2.filter.bool.must.push(data);
        }









        $http.post("http://localhost:9200/_search", genre2).success(function(temp3) {
            $scope.test = temp3;
            $scope.bigTotalItems = $scope.test.hits.total;
        })
        if ($scope.test.hits.total == 0) {
            $scope.noResult = "There is no result your filter try again ....."
        }
    }


    //suggestions
    $scope.check = false;
    $scope.suggestions = {};
    $scope.SuggestFunc = function() {
        var query = {
            "my_suggestion": {
                "text": $scope.filmName,
                "term": {
                    "field": "Title"
                }
            }
        }
        $http.post("http://localhost:9200/_suggest", query).success(function(response) {
            $scope.result = response;
            $scope.check = true;
            $scope.SuggestComplete();
        })
    }

    $scope.given = [];
    $scope.suggestcmp = [];
    $scope.states = [];
    $scope.SuggestComplete = function() {
        for (var i = 0; i < $scope.result.my_suggestion.length; i++) {
            for (var j = 0; j < $scope.result.my_suggestion[i].options.length; j++) {
                $scope.suggestcmp.push($scope.result.my_suggestion[i].options[j].text);
            };
            if ($scope.result.my_suggestion[i].options.length == 0) {
                $scope.given.push($scope.result.my_suggestion[i]);
            }
        };
        var query = {
            "query": {
                "bool": {
                    "should": [],
                    "minimum_should_match": 1
                }
            },
            "size": 5,
            "sort": [{
                "_score": {
                    "order": "desc",
                    "mode": "avg"
                }
            }]
        }
        for (var i = 0; i < $scope.suggestcmp.length; i++) {
            //console.log($scope.suggestcmp[i]);
            var data = {
                "match": {
                    "Title": $scope.suggestcmp[i]
                }
            };
            query.query.bool.should.push(data);
        }
        for (var i = 0; i < $scope.given.length; i++) {
            //console.log($scope.given[i].text);
            var data = {
                "match": {
                    "Title": $scope.given[i].text
                }
            };
            query.query.bool.should.push(data);
        }
        $http.post("http://localhost:9200/_search", query).success(function(response) {
            $scope.suggestions = response;
            //console.log($scope.suggestions.hits.hits[0]._source.Title);
            for (var i = $scope.suggestions.hits.hits.length - 1; i >= 0; i--) {
                $scope.states.push($scope.suggestions.hits.hits[i]._source.Title);
            };
        })

        var fixed_length = $scope.given.length;
        for (var i = 0; i < fixed_length; i++) {
            $scope.given.pop();
        }
        fixed_length = $scope.suggestcmp.length;
        for (var i = 0; i < fixed_length; i++) {
            $scope.suggestcmp.pop();
        }

        //console.log("states");
        //console.log($scope.states);
    }

}])


myApp.controller('detailCtrl', ['$scope', '$http', '$rootScope', '$routeParams', function($scope, $http, $rootScope, $routeParams) {

    var film = {};
    film = {



            "query": {
                "term": {
                    "ID": $routeParams.id
                }

            }
        }
        //console.log($routeParams.id);
    $scope.imdblink = "http://www.imdb.com/title/tt0" + $routeParams.id + "/";
    $http.post("http://localhost:9200/_search", film).success(function(temp3) {
        $scope.test = temp3;
    })


}])



myApp.controller('chartCtrl', ['$scope', '$http', '$rootScope', '$routeParams', function($scope, $http, $rootScope, $routeParams) {


    // default values
    $scope.test2 = {};
    $scope.formats = ["AreaChart", "PieChart", "ColumnChart", "LineChart", "Table", "BarChart", "GeoChart", "Histogram"];
    $scope.charts = ["Year", "Country", "Rating", "Genre"];
    $scope.currentChart=$scope.charts[0];
    $scope.addCharts=[];
    $scope.format = $scope.formats[0];
    $scope.grids = 10;




    $scope.charGenerator = function(Name1, Name2) {

        var chart = {
            "type": $scope.format,
            "displayed": true,
            "data": {
                "cols": [{
                    "id": Name1,
                    "label": Name1,
                    "type": "string",
                    "p": {}
                }, {
                    "id": Name2 + "Numbers",
                    "label": Name2 + "NUmbers",
                    "type": "number",
                    "p": {}
                }],
                "rows": [


                ]
            },
            "options": {
                "title": Name2 + "  Numbers  For " + Name1,
                "isStacked": "true",
                "fill": 20,
                'is3D': true,
                'legend': 'left',
                "background-color": "red",
                "displayExactValues": true,
                "vAxis": {
                    "title": Name1 + " unit",
                    "gridlines": {
                        "count": $scope.grids
                    }
                },
                "hAxis": {
                    "title": Name1
                }
            },
            "formatters": {}
        }
        return chart;

    }
    $scope.chart = function() {

        $scope.yearsChart = $scope.charGenerator("Year", "Film");

        $scope.imdbVotesChart = $scope.charGenerator("Rating", "Film");
        $scope.size=12;
        $scope.countryChart = $scope.charGenerator("Country", "Film");
        $scope.genreChart = $scope.charGenerator("Genre", "Film");




        var query = {
            "query": {
                "match_all": {}
            },
            "size": 0,
            "aggregations": {
                "years": {
                    "histogram": {
                        "field": "Year",
                        "interval": 10
                    }
                }
            }
        }
        $http.post("http://localhost:9200/_search", query).success(function(res) {
            $scope.test2 = res;
            //console.log($scope.test2);
            for (var i = 0; i < $scope.test2.aggregations.years.buckets.length; i++) {



                var c = {
                    "c": []
                };
                var v = {
                    "v": $scope.test2.aggregations.years.buckets[i].key
                };
                var v2 = {
                    "v": $scope.test2.aggregations.years.buckets[i].doc_count
                };
                c.c.push(v);
                c.c.push(v2);
                $scope.yearsChart.data.rows.push(c);



            }
        })



        var queryVotes = {
            "query": {
                "match_all": {}
            },
            "size": 0,
            "aggregations": {
                "imdbVotes": {
                    "histogram": {
                        "field": "imdbVotes",
                        "interval": 1
                    }
                }
            }
        }

        $http.post("http://localhost:9200/_search", queryVotes).success(function(res) {
            $scope.test3 = res;
            //console.log($scope.test3);
            for (var i = 0; i < $scope.test3.aggregations.imdbVotes.buckets.length; i++) {



                var c = {
                    "c": []
                };
                var v = {
                    "v": $scope.test3.aggregations.imdbVotes.buckets[i].key
                };
                var v2 = {
                    "v": $scope.test3.aggregations.imdbVotes.buckets[i].doc_count
                };
                c.c.push(v);
                c.c.push(v2);
                $scope.imdbVotesChart.data.rows.push(c);



            }
        })



        var countryQuery = {
            "query": {
                "match_all": {}
            },
            "size": 0,
            "aggregations": {
                "group_by_country": {
                    "terms": {
                        "field": "Awards"
                    }
                }
            }
        };
        $http.post("http://localhost:9200/_search", countryQuery).success(function(res) {
            $scope.test4 = res;
            //console.log($scope.test4);
            for (var i = 0; i < $scope.test4.aggregations.group_by_country.buckets.length; i++) {



                var c = {
                    "c": []
                };
                var v = {
                    "v": $scope.test4.aggregations.group_by_country.buckets[i].key
                };
                var v2 = {
                    "v": $scope.test4.aggregations.group_by_country.buckets[i].doc_count
                };
                c.c.push(v);
                c.c.push(v2);
                $scope.countryChart.data.rows.push(c);



            }
        })

        var genreQuery = {
            "query": {
                "match_all": {}
            },
            "size": 0,
            "aggregations": {
                "group_by_country": {
                    "terms": {
                        "field": "Genre"
                    }
                }
            }
        }

        $http.post("http://localhost:9200/_search", genreQuery).success(function(res) {
            $scope.test5 = res;
            //console.log($scope.test5);
            for (var i = 0; i < $scope.test5.aggregations.group_by_country.buckets.length; i++) {



                var c = {
                    "c": []
                };
                var v = {
                    "v": $scope.test5.aggregations.group_by_country.buckets[i].key
                };
                var v2 = {
                    "v": $scope.test5.aggregations.group_by_country.buckets[i].doc_count
                };
                c.c.push(v);
                c.c.push(v2);
                $scope.genreChart.data.rows.push(c);



            }

        })











    }
    $scope.chart();
    $scope.chartPanels=[];
    $scope.addChart=function(chartCome){
  		
    	if(chartCome=="Year" && $scope.addCharts.indexOf($scope.yearsChart)==-1){
    		$scope.addCharts.push($scope.yearsChart);
    		$scope.chartPanels.push($scope.panelType);
    		$scope.errors=false;
    	}
    	else if(chartCome=="Genre" && $scope.addCharts.indexOf($scope.genreChart)==-1){
    		$scope.addCharts.push($scope.genreChart);
    		$scope.chartPanels.push($scope.panelType);
    		$scope.errors=false;
    	}
    	else if(chartCome=="Country" && $scope.addCharts.indexOf($scope.countryChart)==-1){
    		$scope.addCharts.push($scope.countryChart);
    		$scope.chartPanels.push($scope.panelType);
    		$scope.errors=false;
    	}
    	else if(chartCome=="Rating" && $scope.addCharts.indexOf($scope.imdbVotesChart)==-1){
    		$scope.addCharts.push($scope.imdbVotesChart);
    		$scope.chartPanels.push($scope.panelType);
    		$scope.errors=false;
    	}
    	else
    		$scope.errors=true;
    }
    
    $scope.deleteChart=function(index){
    	$scope.addCharts.splice(index,1);
    	$scope.chartPanels.splice(index,1);
    		
    }
    $scope.alert ={ type: 'danger', msg: 'This Chart Already Added Please add another one.' }
  $scope.closeAlert = function(index) {
    $scope.errors=false;
  };
  	
  	$scope.errors=false;
    $scope.panels=["primary","success","danger","info","warning"];
    $scope.panelType=$scope.panels[0];


}])
